# Create by gravifoxita #

import requests

def check_security(url):

    response = requests.get(url)

    if response.status_code == 200:
        if "<" in response.text or ">" in response.text:
            print("Alerte de sécurité XSS détectée pour", url)
        
        if "SELECT" in response.text or "UNION" in response.text:
            print("Alerte de sécurité SQL Injection détectée pour", url)
        
        if ";" in response.text or "&" in response.text:
            print("Alerte de sécurité Command Injection détectée pour", url)
        
        if "../" in response.text or "..\\" in response.text:
            print("Alerte de sécurité Path Traversal détectée pour", url)
        
        if "https//" in response.text or "http//" in response.text:
            print("Alerte de sécurité Remote File Inclusion détectée pour", url)
        
        if "ftp://" in response.text or "ftps://" in response.text:
            print("Alerte de sécurité FTP Inclusion détectée pour", url)
        
        if "file://" in response.text or "files://" in response.text:
            print("Alerte de sécurité File Inclusion détectée pour", url)
        
        if "\n" in response.text or "\r" in response.text:
            print("Alerte de sécurité HTTP Header Injection détectée pour", url)
        
        if "WAITFOR" in response.text:
            print("Alerte de sécurité Blind SQL Injection détectée pour", url)
        
        if "or" in response.text or "and" in response.text:
            print("Alerte de sécurité Log Injection détectée pour", url)
        
        if "PHPSESSID" in response.text:
            print("Alerte de sécurité Session Fixation détectée pour", url)
        
        if "<!DOCTYPE" in response.text or "<?xml" in response.text:
            print("Alerte de sécurité XML Injection détectée pour", url)
    
    else:
        print("Impossible de check", url, "car erreur", response.status_code, ":(")

if __name__ == "__main__":
    check_security("input_your_url_website")